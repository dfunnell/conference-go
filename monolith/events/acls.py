from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json
import random


def get_location_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    params = {
        "query": city + " " + state,
        "per_page": 5
        }
    headers = {'Authorization': PEXELS_API_KEY}
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][random.randint(0,4)]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_conference_weather(city, state):
    url = "http://api.openweathermap.org/geo/1.0/direct"
    headers = {'Authorization': OPEN_WEATHER_API_KEY}
    params = {
        "q": city + "," + state + "," + "1",
        "appid": OPEN_WEATHER_API_KEY,
        "limit": 1
        }
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)

    try:
        lat = content[0]["lat"]
        lon = content[0]["lon"]
    except (KeyError, IndexError):
        return "null, Error with lat/lon"  # <- could have just put None too

    url = "https://api.openweathermap.org/data/2.5/weather"
    headers = {'Authorization': OPEN_WEATHER_API_KEY}
    params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
        "lang": "en",
        }
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"temp": f'{content["main"]["temp"]} degrees fahrenheit',
            "description": content["weather"][0]["description"]}
    except (KeyError, IndexError):
        return None  # <- wanted to put error with temp/decription
